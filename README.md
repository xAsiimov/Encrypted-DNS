# Encrypted-DNS
[![alt text](https://img.shields.io/github/license/Siujoeng-Lau/Encrypted-DNS.svg)](https://github.com/Siujoeng-Lau/Encrypted-DNS/blob/master/LICENSE)
---

Python-based DNS server and forwarder.

Licensed under Apache License 2.0

Runtime Environment: [Python 3](https://www.python.org/downloads/)
